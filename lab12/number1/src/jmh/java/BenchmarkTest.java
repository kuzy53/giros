
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;
import java.util.List;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Fork;

@Fork(value = 3, warmups = 2)
@Warmup(iterations = 2)
@BenchmarkMode(Mode.AverageTime)
public class BenchmarkTest {

    private static Map<String, Integer> hashMap;
    private static Map<String, Integer> treeMap;
    private static Map<String, Integer> hashTable;
    private static Map<String, Integer> linkedHashMap;
    private static List hashMapToList;
    private static List hashTableToList;
    private static List linkedHashMapToList;
    private static List treeMapToList;

    @State(Scope.Benchmark)
    public static class BenchmarkState{
        @Setup(Level.Invocation)
        public static void setupMaps(){
            hashMap = new HashMap<>();
            hashTable = new Hashtable<>();
            linkedHashMap = new LinkedHashMap<>();
            treeMap = new TreeMap<>();
        }

        @Setup(Level.Trial)
        public static void setupLists(){
            hashMapToList = Structure1.toList(Structure1.generate(new HashMap<>()));
            hashTableToList = Structure1.toList(Structure1.generate(new Hashtable<>()));
            linkedHashMapToList = Structure1.toList(Structure1.generate(new LinkedHashMap<>()));
            treeMapToList = Structure1.toList(Structure1.generate(new TreeMap<>()));
        }
    }

    @Benchmark
    public static void testGenerateTimeHashMap() {
        printClass(hashMap);
        Structure1.generate(hashMap);
    }

    @Benchmark
    public static void testGenerateTimeHashTable() {
        printClass(hashTable);
        Structure1.generate(hashTable);
    }

    @Benchmark
    public static void testGenerateTimeLinkedHashMap() {
        printClass(linkedHashMap);
        Structure1.generate(linkedHashMap);
    }

    @Benchmark
    public static void testGenerateTimeTreeMap() {
        printClass(treeMap);
        Structure1.generate(treeMap);
    }

    @Benchmark
    public static void testToListTimeHashMap() {
        printClass(hashMap);
        Structure1.toList(hashMap);
    }

    @Benchmark
    public static void testToListTimeHashTable() {
        printClass(hashTable);
        Structure1.toList(hashTable);
    }

    @Benchmark
    public static void testToListTimeLinkedHashMap() {
        printClass(linkedHashMap);
        Structure1.toList(linkedHashMap);
    }

    @Benchmark
    public static void testToListTimeTreeMap() {
        printClass(treeMap);
        Structure1.toList(treeMap);
    }

    @Benchmark
    public static void testSortTimeHashMap() {
        printClass(hashMap);
        Structure1.sort(hashMapToList);
    }

    @Benchmark
    public static void testSortTimeHashTable() {
        printClass(hashTable);
        Structure1.sort(hashTableToList);
    }

    @Benchmark
    public static void testSortTimeLinkedHashMap() {
        printClass(linkedHashMap);
        Structure1.sort(linkedHashMapToList);
    }

    @Benchmark
    public static void testSortTimeTreeMap() {
        printClass(treeMap);
        Structure1.sort(treeMapToList);
    }

    @Benchmark
    public static void testAnalyzeOverTimeHashMap() {
        Structure1.analyzeOverTime(new HashMap<String, Integer>());
    }

    @Benchmark
    public static void testAnalyzeOverTimeHashTable() {
        Structure1.analyzeOverTime(new Hashtable<String, Integer>());
    }

    @Benchmark
    public static void testAnalyzeOverTimeLinkedHashMap() {
        Structure1.analyzeOverTime(new LinkedHashMap<String, Integer>());
    }

    @Benchmark
    public static void testAnalyzeOverTimeTreeMap() {
        Structure1.analyzeOverTime(new TreeMap<String, Integer>());
    }

    private static void printClass(Map map){
        System.out.println("Класс: " + map.getClass().getSimpleName());
    }

}

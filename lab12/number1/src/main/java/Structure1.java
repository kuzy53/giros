import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

/**
 * Класс входа
 * @autor Кузнецов Никита
 */
public class Structure1 {

    /** Поле размер экземпляра Map */
    private static final int MAP_SIZE = 2000000;

    /** Поле максимальное значение элемента */
    private static final int MAP_MAXVALUE = 100000;

    /** Поле длина ключа */
    private static final int MAP_KEY_LENGTH = 10;

    /** Поле знаменатель для приведения времени к секундам */
    private static final int TIME_DIVISION = 1000;

    /** Поле используемый алфавит */
    private static final String ALPHABET = "abcdefghijk";

    /**
     * точка входа
     * @param args - массив входных строк
     */
    public static void main(String[] args) {

        analyzeOverTime(new LinkedHashMap<String, Integer>());
        analyzeOverTime(new HashMap<String, Integer>());
        analyzeOverTime(new Hashtable<String, Integer>());
        analyzeOverTime(new TreeMap<String, Integer>());

    }

    /**
     * метод временного анализа генерации, сбора и сортировки элементов из экземпляра Map
     * @param map - экземпляр класса Map
     */
    public static void analyzeOverTime(Map map) {

        System.out.println("Класс: " + map.getClass().getSimpleName());

        double generalStartTime = System.currentTimeMillis();
        double actoinStartTime = System.currentTimeMillis();

        generate(map);

        printExecutionTime("Время генерации", actoinStartTime);

        actoinStartTime = System.currentTimeMillis();

        List list = toList(map);

        printExecutionTime("Время сбора", actoinStartTime);

        actoinStartTime = System.currentTimeMillis();

        sort(list);

        printExecutionTime("Время сортировки", actoinStartTime);

        printExecutionTime("Время общее", generalStartTime);

        System.out.println();

    }

    /**
     * метод временного анализа генерации, сбора и сортировки элементов из экземпляра Map
     * @param map - экземпляр класса Map
     */
    public static Map generate(Map map){

        for (int i = 0; i < MAP_SIZE; i++) {
            map.put(getRandomString(), (int) (Math.random() * MAP_MAXVALUE));
        }

        return map;
    }

    /**
     * метод временного анализа сбора элементов из экземпляра Map и перевод их в экземпляр List
     * @param map - экземпляр класса Map
     * @return возвращает экземпляр List из элементов экземпляра Map
     */
    public static List toList(Map map){
        List<Map.Entry<String,Integer>> list;

        list = new ArrayList<>(map.entrySet());

        return list;
    }

    /**
     * метод временного анализа сортировки списка, полученного из экземпляра Map
     * @param list - список, полученный из экземпляра Map
     */
    public static void sort(List list){

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue()-o2.getValue();
            }
        });
    }

    /**
     * метод генерации случайной строки из символов ALPHABET длины MAP_KEY_LENGTH
     * @return возвращает скгенерированную строку
     */
    private static String getRandomString() {
        int alphabetLength = ALPHABET.length();
        StringBuffer resultSb = new StringBuffer();

        for (int i = 0; i < MAP_KEY_LENGTH; i++) {
            resultSb.append(ALPHABET.charAt((int) (Math.random() * alphabetLength)));
        }

        return resultSb.toString();

    }

    /**
     * метод корректного выведения времени выполнения действия
     * @param comment - название действия
     * @param comment - время начал действия
     */
    private static void printExecutionTime(String comment, double startTime){

        System.out.format("%s: %s sec\n", comment, (System.currentTimeMillis() - startTime)/ TIME_DIVISION);

    }

}

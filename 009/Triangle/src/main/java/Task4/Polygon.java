package Task4;

public class Polygon implements Output {
    protected int perimet;
    protected int[] sides;

    Polygon(int[] sides) {
        this.sides = sides;
        this.perimet = perimeterPol();
        this.checkLen();

    }

    public boolean checkLen() {
        int sum = this.perimet;
        for (int i = 0; i < this.sides.length; i++) {
            if (2 * this.sides[i] > sum) {
                return false;
            }
        }
        return true;
    }


    public int perimeterPol() {
        int perimeter = 0;
        for (int i = 0; i < this.sides.length; i++) {
            perimeter += this.sides[i];
        }
        return perimeter;
    }

    public String output() {
        return this.sides.length + "-угольник, периметр у которого равен - " + this.perimet;
    }
}

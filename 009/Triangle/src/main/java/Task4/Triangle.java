package Task4;

class Triangle extends Polygon {

    Triangle(int[] edges) throws TriangleException {
        super(edges);
        if (edges.length != 3) {
            edges = null;
            perimet = 0;
            throw new TriangleException("Не треугольник");
        }
    }


    public String output() {
        if (perimet == 0 && sides.length != 3) {
            return "Это не треугольник";
        }
        return "Треугольник, периметр у которого равен - " + this.perimet;
    }
}

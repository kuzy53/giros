package Task4;

public class Text implements Output {
    String text;

    Text(String text) {
        this.text = text;
    }

    public String output() {
        return this.text;
    }
}

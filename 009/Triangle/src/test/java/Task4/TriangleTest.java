package Task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TriangleTest {
    @Test
    void perimeterTriangle() throws TriangleException {
        int[] sides = new int[]{3, 5, 8};
        Triangle triangle = new Triangle(sides);
        assertEquals(16, triangle.perimeterPol());
    }

    @Test
    void Triangle() throws TriangleException {
        int[] sides = new int[]{3, 5, 8};
        Triangle triangle = new Triangle(sides);
        assertEquals("Треугольник, периметр у которого равен - 16", triangle.output());
    }

}



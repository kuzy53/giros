package Vector;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @Test
    void vectorPrint() {
        Vector vector = new Vector(5, 5, 10, 10);
        assertEquals("Начало вектора (5;5)\n" +
                "Конец вектора (10;10)", vector.printVector());
    }

    @Test
    void vectorEquals() {
        Vector vector1 = new Vector(3, 3, 5, 5);
        Vector vector2 = new Vector(3, 3, 5, 5);
        assertTrue(vector1.equals(vector2));
    }


    @Test
    void vectorPlus() {
        Vector vector1 = new Vector(1, 1, 5, 5);
        Vector vector2 = new Vector(5, 5, 10, 10);
        Vector vectorExpected = new Vector(1, 1, 10, 10);
        assertEquals(vectorExpected, Vector.sum(vector1, vector2));
    }



    @Test
    void vectorMultiply() {
        Vector vector = new Vector(0, 0, 5, 5);
        Vector vectorExpected = new Vector(0, 0, 20, 20);
        assertEquals(vectorExpected, Vector.multiply(vector, 4));
    }


    @Test
    void vectorMinus() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(2, 2, 3, 3);
        Vector vectorExpected = new Vector(3, 3, 1, 1);
        assertEquals(vectorExpected, Vector.minus(vector1, vector2));
    }


    @Test
    void vectorDivision() {
        Vector vector = new Vector(0, 0, 10, 10);
        Vector vectorExpected = new Vector(0, 0, 2, 2);
        assertEquals(vectorExpected, Vector.division(vector, 5));
    }

}

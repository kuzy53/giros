package Vector;


public class Vector {
    private int startX;
    private int startY;
    private int endX;
    private int endY;

    public Vector(int startX, int startY, int endX, int endY) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }

    public Vector(Vector vector) {
        this.startX = vector.getStartX();
        this.endX = vector.getEndX();
        this.startY = vector.getStartY();
        this.endY = vector.getEndY();
    }


    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getEndX() {
        return endX;
    }

    public int getEndY() {
        return endY;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }


    public void setStartY(int startY) {
        this.startY = startY;
    }


    public void setEndX(int endX) {
        this.endX = endX;
    }


    public void setEndY(int endY) {
        this.endY = endY;
    }

    public String printVector() {
        System.out.println("Начало вектора (" + this.startX + ";" + this.startY + ")" + "\n" +
                "Конец вектора (" + this.endX + ";" + this.endY + ")");
        return null;
    }

    public boolean equals(Object o) {
        if (o.getClass() == this.getClass()) {
            Vector vector = (Vector) o;
            return Double.compare(vector.startX, startX) == 0 && Double.compare(vector.endX, endX) == 0 &&
                    Double.compare(vector.startY, startY) == 0 && Double.compare(vector.endY, endY) == 0;
        }
        return false;
    }

    public static Vector sum(Vector vector1, Vector vector2) {
        Vector vectorNew = new Vector(vector1);
        vectorNew.sum(vector2);
        return vectorNew;
    }

    public Vector sum(Vector vector1) {
        double shiftX = vector1.getEndX() - vector1.getStartX();
        double shiftY = vector1.getEndY() - vector1.getStartY();
        this.endX += shiftX;
        this.endY += shiftY;
        return this;
    }

    public static Vector minus(Vector vector1, Vector vector2) {
        Vector vectorNew = new Vector(vector1);
        vectorNew.minus(vector2);
        return vectorNew;
    }

    public Vector minus(Vector vector1) {
        int countX = this.startX;
        int countY = this.startY;
        this.startX = this.endX + vector1.getEndX() - vector1.getStartX();
        this.startY = this.endY + vector1.getEndY() - vector1.getStartY();
        this.endX = countX;
        this.endY = countY;
        return this;
    }

    public static Vector multiply(Vector vector, int count) {
        Vector vectorNew = new Vector(vector);
        vectorNew.multiply(count);
        return vectorNew;
    }

    public Vector multiply(int count) {
        this.endX *= count;
        this.endY *= count;
        return this;
    }

    public static Vector division(Vector vector, int count) {
        Vector vectorNew = new Vector(vector);
        vectorNew.division(count);
        return vectorNew;
    }

    public Vector division(int count) {
        this.endX /= count;
        this.endY /= count;
        return this;
    }
}


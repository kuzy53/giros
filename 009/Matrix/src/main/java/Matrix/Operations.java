package Matrix;

class Operations{
    public static int[][] MatrixPlus(Matrix matrix01, Matrix matrix02)throws MatrixException {
        int[][] matrix1 = matrix01.getMatrix();
        int[][] matrix2 = matrix02.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new MatrixException("Одна из матриц пуста");
        }
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new MatrixException("Матрицы не одинаковы");
        }
        int[][] resultMatrix = new int[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                resultMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return resultMatrix;
    }

    public static int[][] MatrixMinus(Matrix matrix01, Matrix matrix02)throws MatrixException {
        int[][] matrix1 = matrix01.getMatrix();
        int[][] matrix2 = matrix02.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new MatrixException("Одна из матриц пуста");
        }
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new MatrixException("Матрицы не одинаковы");
        }
        int[][] resultMatrix = new int[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                resultMatrix[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        }
        return resultMatrix;
    }

    public static int[][] multiplyMatrix(Matrix matrix01, Matrix matrix02)throws MatrixException {
        int[][] matrix1 = matrix01.getMatrix();
        int[][] matrix2 = matrix02.getMatrix();
        if (matrix1 == null || matrix2 == null) {
            throw new MatrixException("Одна из матриц пуста");
        }
        if (matrix1[0].length != matrix2.length) {
            throw new MatrixException("Матрицы невозможно множить");
        }
        int[][] resultMatrix = new int[matrix1.length][matrix2[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                for (int z = 0; z < matrix2.length; z++) {
                    resultMatrix[i][j] += matrix1[i][z] * matrix2[z][j];
                }
            }
        }
        return resultMatrix;
    }

}
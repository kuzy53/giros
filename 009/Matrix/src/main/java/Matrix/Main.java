package Matrix;

class Main{
    public static void main(String[] args) throws MatrixException {
        Matrix matrix = new Matrix(3,3);
        Matrix matrix2 = new Matrix(3,3);
        System.out.println("Сумма матриц");
        int[][] sumResult = Operations.MatrixPlus(matrix, matrix2);
        for (int i = 0; i < sumResult.length; i++) {
            for (int j = 0; j < sumResult[0].length; j++) {
                System.out.print(sumResult[i][j] + "   ");
            }
            System.out.println();
        }
        System.out.println("Разность матриц");
        int[][] subtractionResult = Operations.MatrixMinus(matrix, matrix2);
        for (int i = 0; i < subtractionResult.length; i++) {
            for (int j = 0; j < subtractionResult[0].length; j++) {
                System.out.print(subtractionResult[i][j] + "   ");
            }
            System.out.println();
        }
        System.out.println("Умножение матриц");
        int[][] multiplyResult = Operations.multiplyMatrix(matrix, matrix2);
        for (int i = 0; i < multiplyResult.length; i++) {
            for (int j = 0; j < multiplyResult[0].length; j++) {
                System.out.print(multiplyResult[i][j] + "   ");
            }
            System.out.println();
        }


    }
}
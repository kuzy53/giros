package Matrix;


public class MatrixException extends Exception{
    String message;

    public MatrixException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}

package Matrix;

class Matrix{
    private int[][] matrix;

    Matrix(int string, int column){
        this.matrix = new int[string][column];
        generateMatrix();
    }

    Matrix() {

    }

    public int[][] getMatrix(){
        return this.matrix;
    }

    public void setMatrix(int[][] matrix1)throws MatrixException{
        if (matrix1.length == this.matrix.length) {
            for (int i = 0;i < matrix1.length ;i++ ) {
                if (matrix1[i].length != this.matrix[0].length) {
                    throw new MatrixException("несоответствия строк");
                }
            }
        }
        else {
            throw new MatrixException("несоответствия столбцов");
        }
        for (int i = 0;i < this.matrix.length ;i++ ) {
            for (int j = 0;j < this.matrix[0].length ;j++ ) {
                this.matrix[i][j] = matrix1[i][j];
            }
        }
    }

    public void generateMatrix(){
        for (int i = 0;i < this.matrix.length ;i++ ) {
            for (int j = 0;j < this.matrix[0].length ;j++ ) {
                this.matrix[i][j] = (int)(Math.random()* 10);
            }
        }
    }
}
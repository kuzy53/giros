package Matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class MatrixTest {

    @Test
    void GenerateMatrix() throws MatrixException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}};
        Matrix matrix = new Matrix(2, 2);
        matrix.setMatrix(m1);
        assertArrayEquals(m1, matrix.getMatrix());
    }

}

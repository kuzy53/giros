package Matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OperationTest {
    @Test void sum()throws MatrixException{
        int[][] matrix01 = new int[][]{{1, 2, 3}, {4, 5, 6}};
        int[][] matrix02 = new int[][]{{1, 2, 3}, {4, 5, 6}};
        Matrix matrix1 = new Matrix(2, 3);
        Matrix matrix2 = new Matrix(2, 3);
        matrix1.setMatrix(matrix01);
        matrix2.setMatrix(matrix02);
        int[][] result = new int[][]{{2, 4, 6}, {8, 10, 12}};
        assertArrayEquals(result, Operations.MatrixPlus(matrix1, matrix2));
    }
    @Test void subtraction()throws MatrixException{
        int[][] matrix01 = new int[][]{{2, 0}, {-3, 5}, {4, 1}};
        int[][] matrix02 = new int[][]{{1, 7}, {2, -3}, {4, 6}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(3, 2);
        matrix1.setMatrix(matrix01);
        matrix2.setMatrix(matrix02);
        int[][] result = new int[][]{{1, -7}, {-5, 8}, {0, -5}};
        assertArrayEquals(result, Operations.MatrixMinus(matrix1, matrix2));
    }
    @Test void multiply()throws MatrixException{
        int[][] matrix01 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] matrix02 = new int[][]{{1, 2, 3}, {4, 5, 6}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(2, 3);
        matrix1.setMatrix(matrix01);
        matrix2.setMatrix(matrix02);
        int[][] result = new int[][]{{9, 12, 15}, {19, 26, 33}, {29, 40, 51}};
        assertArrayEquals(result, Operations.multiplyMatrix(matrix1, matrix2));
    }


}

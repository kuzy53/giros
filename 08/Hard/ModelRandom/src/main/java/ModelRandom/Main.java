package ModelRandom;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Введите желаемый порог: ");
        double numberThreshold = sc.nextDouble();

        System.out.print("Введите желаемый коэфициент: ");
        double coefficient = sc.nextDouble();

        System.out.print("Введите желаемое время максимальной обработки: ");
        int max_time = sc.nextInt();

        ModelRandom model = new ModelRandom();
        model.process(numberThreshold, coefficient, max_time);
        System.out.println("время от начала эксперимента: " + model.time + "мс" + "; Число: " + model.number);

    }
}

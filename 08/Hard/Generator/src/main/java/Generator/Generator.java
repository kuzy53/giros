package Generator;


import java.util.Random;
import java.util.Date;

class Generator {

    private int[] randomNumber;
    private long timeStart;
    private long timeOut;
    private int count;


    Generator(int count, int timeOut) {
        this.count = count;
        this.randomNumber = new int[this.count];
        this.timeOut = timeOut;
        this.generate();
    }

    private void generate() {
        long start = new Date().getTime();
        for (int i = 0; i < this.count; i++) {
            this.randomNumber[i] = new Random().nextInt();
        }
        this.timeStart = new Date().getTime() - start;
    }

    public boolean getSuccess() {
        return this.timeStart < this.timeOut;
    }

    public int[] getNumbers() {
        if (this.getSuccess() == true) {
            return this.randomNumber;
        } else {
            return null;
        }
    }
}
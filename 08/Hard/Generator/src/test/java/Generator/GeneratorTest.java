package Generator;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class GeneratorTest {
    @Test
    void generatorTrue() {
        Generator generator = new Generator(1000, 5);
        assertTrue(generator.getSuccess());
        assertEquals(1000, generator.getNumbers().length);
    }


    @Test
    void generatorNull(){
        Generator generator = new Generator(10000000, 5);
        assertNull(generator.getNumbers());
    }
}

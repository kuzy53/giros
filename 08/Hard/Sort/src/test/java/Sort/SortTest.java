package Sort;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SortTest {
    @Test
    void testSort() {
        double[] arr = {2, 8, 5, 3};
        assertArrayEquals(new double[] {2, 3, 5, 8}, Sort.sort(arr));
    }

    @Test void testCreateSort(){
        double[] arr = {2, 8, 5, 3};
        assertArrayEquals(new double[] {2, 3, 5, 8}, Sort.createSortedArray(arr));

    }
}

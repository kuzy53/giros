package Sort;

class Sort {

    public static double[] sort(double[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    double t = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = t;
                }
            }
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        return a;
    }

    public static double[] createSortedArray(double[] a) {
        double[] b = a.clone();
        sort(b);
        return b;
    }
}
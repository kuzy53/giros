package Matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatrixTest {

    @Test
    void matrixOne() throws Exception {
        int[][] matBase = {{1}};
        int[][] matMultiplier = {{1}};
        int[][] matExpected = {{1}};
        assertArrayEquals(matExpected, Matrix.multiplyMatrix(matBase, matMultiplier));
    }
    @Test
    void matrixZero() throws Exception {
        int[][] matBase = {{0}};
        int[][] matMultiplier = {{0}};
        int[][] matExpected = {{0}};
        assertArrayEquals(matExpected, Matrix.multiplyMatrix(matBase, matMultiplier));
    }


    @Test
    void matrixMultiply() throws Exception {
        int[][] matBase = {{33,34,12},
                {33,19,10},
                {12,14,17},
                {84,24,51},
                {43,71,21}};
        int[][] matMultiplier = {{10,11,34,55},
                {33,45,17,81},
                {45,63,12,16}};
        int[][] matExpected = {{1992, 2649, 1844, 4761}, {1407, 1848, 1565, 3514}, {1347, 1833, 850, 2066}, {3927, 5217, 3876, 7380}, {3718, 4991, 2921, 8452}};
        assertArrayEquals(matExpected, Matrix.multiplyMatrix(matBase, matMultiplier));
    }
    }


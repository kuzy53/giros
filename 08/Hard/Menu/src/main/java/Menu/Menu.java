package Menu;
import java.util.Scanner;


class Menu {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int userResponse = 0;
        Messages begin = new Messages();
        Messages level1 = new Messages();
        Messages level1_2 = new Messages();
        Messages level2 = new Messages();
        Messages level2_2 = new Messages();
        Action one = new Action("Вывести тайное сообщение 1: ");
        one.setMessage("Сдам восьмую лабу!");
        Action two = new Action("\"Вывести тайное сообщение 2: ");
        two.setMessage("Проект на стадии завершения");
        Action three = new Action("Вывести тайное сообщение 3: ");
        three.setMessage("9 Лаба почти готова");
        Action four = new Action("Вывести тайное сообщение 4: ");
        four.setMessage("Структуры в процессе сдачи");
        Action five = new Action("Вывести тайное сообщение 5: ");
        five.setMessage("Хочу получить допуск!!!");
        begin.connection(level1);
        begin.connection(level1_2);
        begin.setAction(one);
        level1.connection(level2);
        level1.connection(level2_2);
        level1.setAction(two);
        level1_2.setAction(three);
        level2.setAction(four);
        level2_2.setAction(five);

        while(true){
            begin.printStr();
            userResponse = in.nextInt();
            System.out.println();
            if (userResponse == 0){
                if (begin.parent == null){
                    break;
                }
                else {
                    begin = begin.parent;
                }
            }

            if(userResponse > 0 && userResponse < begin.actions.size() + begin.children.size()){
                begin = begin.children.get(userResponse - begin.children.size() + 1);
            } else if(userResponse > begin.children.size() && userResponse <= begin.actions.size() + begin.children.size()){
                begin.actions.get(0).act();
            }
        }
    }
}

package Menu;

public class Action{
    String str;
    String message;

    public Action(String str){
        this.str = str;
    }

    public void act(){
      System.out.println(this.message);
    }

    public String getStr(){
        return this.str;
    }

    public void setMessage(String message){
      this.message = message;
    }
}

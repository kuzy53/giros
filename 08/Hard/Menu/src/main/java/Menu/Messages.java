package Menu;


import java.util.ArrayList;

public class Messages {
    ArrayList<Messages> children = new ArrayList<Messages>();
    Messages parent;
    ArrayList<Action> actions = new ArrayList<Action>();

    public void setAction(Action action){
        this.actions.add(action);
    }

    public void connection(Messages nextLevel){
        this.children.add(nextLevel);
        nextLevel.parent = this;
    }

    public void printStr(){
        System.out.println("Действия с меню:");
        if (this.parent == null){
            System.out.println("0 Выйти");
        }
        else{
            System.out.println("0 Вернуться на уровень назад");
        }
        int num = 1;
        for (int i = 0; i < this.children.size(); i++) {
            System.out.println(num + " " + "Перейти на следующий уровень");
            num++;
        }
        System.out.println("Вывод тайного сообщения:");
        for (int i = 0; i < this.actions.size(); i++) {
            System.out.println(num + " " + this.actions.get(i).getStr());
            num++;
        }
        System.out.println();
    }
}

package Intersections;

import java.util.Scanner;
import java.util.Random;

public class Intersections{

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите количество повторений: ");
        int repeat = sc.nextInt();

        System.out.println("Введите размер области: ");
        int area = sc.nextInt();

        System.out.println("Введите сдвиг относительно начала координат: ");
        int change = sc.nextInt();

        int i = 0;
        int counter = 0;
        while (i < repeat) {
            int first = change + new Random().nextInt(area);
            int second = change + new Random().nextInt(area);
            int third = change + new Random().nextInt(area);
            int fourth = change + new Random().nextInt(area);

            if ((first < third && third < second) || (first < fourth && fourth < second) ||
                    (third < first && first < fourth) || (third < second && second < fourth) || (second < third && third < fourth) || (second < fourth && fourth < first) ||
                    (third < second && first < fourth)){
                counter++;
            }
            i++;
        }
        System.out.println(counter);
    }
}
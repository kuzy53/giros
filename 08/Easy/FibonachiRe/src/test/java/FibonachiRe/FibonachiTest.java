package FibonachiRe;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibonachiTest {
    @Test void Memo() {
        long[] arr = new long[7];
        FibonachiRe.fib(7, arr);
        assertArrayEquals(new long[] {0,1,1,2,3,5,8}, arr);
    }
}

package SymbolQuantity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SymbolQuantityTest{
    @Test void testSymbEmpty(){
        assertEquals(0, SymbolQuantity.quan('a', ""));
    }
    @Test void testSymb(){
        assertEquals(5, SymbolQuantity.quan('a', "aaaaa"));
    }
}

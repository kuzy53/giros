package Fibonachi;

public class Fibonachi{
    public static void main(String[] args){
        System.out.println(fibo(5));
    }
    public static int[] fibo(int quan){
        int first = 0;
        int second = 1;
        int[] resultArr = new int[quan];
        if (resultArr.length > 2){
            resultArr[0] = 0;
            resultArr[1] = 1;
            for(int i = 2; i < quan; i++){
                int result = first + second;
                resultArr[i] = result;
                first = second;
                second = result;
            }
            return resultArr;
        }
        if (resultArr.length == 2){
            return new int[]{0, 1};
        } else {
            return new int[]{0};
        }
    }
}
package Fibonachi;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibonachiTest{
    @Test void testFibonachiEmpty(){
        assertArrayEquals(new int[]{0}, Fibonachi.fibo(0));
    }
    @Test void testFibonachi(){
        assertArrayEquals(new int[]{0,1,1,2,3}, Fibonachi.fibo(5));
    }
}


package Factorial;
public class Factorial{
    public static void main(String[] args){
        factorial(10);
        System.out.println(factorial(100000000));
    }


    public static long factorial(int max){
        long a = 1;
        for(int i = 1; i <= max; i++){
            a = a*i;
        }
        return a;
    }
}
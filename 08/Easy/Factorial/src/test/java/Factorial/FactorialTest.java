package Factorial;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FactorialTest{
    @Test void testFactorialZero(){
        assertEquals(1, Factorial.factorial(0));
    }
    @Test void testFactorial(){
        assertEquals(720, Factorial.factorial(6));
    }
}

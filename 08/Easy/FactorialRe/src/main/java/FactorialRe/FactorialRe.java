package FactorialRe;

class FactorialRe{
    public static void main(String[] args){
        System.out.printf("%d \n", fact(5000000));

    }
    public static long fact(int a){
        if(a==1){
            return 1;
        }else{
            return a*fact(a-1);
        }
    }
}

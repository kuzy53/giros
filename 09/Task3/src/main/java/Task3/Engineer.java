package Task3;

public class Engineer extends GeneralEmployee {

    Engineer(String name, int money) {
        super(name, money);
    }

    public String engineer() {
        return "Я занимаюсь инженерным делом";
    }

    public String toString() {
        return "Мое имя - " + name + ". Моя должность - инженер" + ". Моя зарплата - " + salary + " Российских рублей.";
    }


}

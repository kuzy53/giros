package Task3;

public class ChiefAccountant extends Accountant {

    ChiefAccountant(String name, int money) {
        super(name, money);
    }

    public String accountant() {
        return "А я - главный среди бугалтеров";
    }

    public void changeSalary(GeneralEmployee employer, int money) {
        employer.salary = money;
    }

    public String toString() {
        return "Мое имя - " + name + ". Моя должность - главный бухгалтер" + ". Моя зарплата - " + salary + " Российских рублей.";
    }

}

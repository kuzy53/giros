package Task3;

public class Worker extends GeneralEmployee {

    Worker(String name, int money) {
        super(name, money);
    }

    public String worker() {
        return "Я обычный работник";
    }

    public String toString() {
        return "Мое имя - " + name + ". Моя должность - работник" + ". Моя зарплата - " + salary + " Российских рублей.";
    }

}

package Task3;

public class GeneralEmployee {
    protected String name;
    protected int salary;

    public GeneralEmployee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public String employee() {
        return "Я один из сотрудников нашей компании";
    }

    public String toString() {
        return "Мое имя - " + name + ". Моя должность - сотрудник" + ". Моя зарплата - " + salary + " Российских рублей.";
    }

    public int hashCode() {
        return name.length() + salary * 37;
    }

    public boolean equals(Object obj) {
       GeneralEmployee lol = (GeneralEmployee) obj;
        if (obj.getClass() == this.getClass()) {
            return this.name == lol.getName() &&
                    this.salary == lol.getSalary();
        }
        return false;
    }


    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}

package Task3;

public class Accountant extends GeneralEmployee {

    Accountant(String name, int money){
        super(name, money);
    }

    public String accountant(){
        return "Я работаю бугалтером";
    }

    public String toString() {
        return "Мое имя - " + name + ". Моя должность - бухгалтер" + ". Моя зарплата - " + salary + " Российских рублей.";
    }
}

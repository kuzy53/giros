package Task3;

class Main {
    public static void main(String[] args) {
        GeneralEmployee[] employee = new GeneralEmployee[7];

        employee[0] = new Accountant("Ренат", 80000);
        employee[1] = new ChiefAccountant("Басков", 1300000);
        employee[2] = new Engineer("Ахмет", 75000);
        employee[3] = new Worker("Равшан", 40000);
        employee[4] = new GeneralEmployee("Магамед", 10000);
        employee[5] = new Engineer("Джамшут", 75000);
        employee[6] = new Worker("Витя", 40000);
        for (int i = 0; i < employee.length; i++) {
            System.out.println(employee[i]);
        }
    }

}

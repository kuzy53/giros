package Task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EngineerTest {
    @Test
    void engineer() {
        Engineer engineer = new Engineer("Ахмет", 75000);
        assertEquals("Я занимаюсь инженерным делом", engineer.engineer());
    }

    @Test
    void engineerToString() {
        Engineer engineer = new Engineer("Ахмет", 75000);
        assertEquals("Мое имя - Ахмет. Моя должность - инженер. Моя зарплата - 75000 Российских рублей.", engineer.toString());
    }
}

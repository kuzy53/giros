package Task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {

    @Test
    void equals() {
        Worker worker = new Worker("Равшан", 40000);
        Worker worker1 = new Worker("Равшан", 40000);
        assertTrue(worker.equals(worker1));
    }

    @Test
    void hashCodes() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertEquals(1480006, employee.hashCode());
    }

    @Test
    void clones() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertNull(employee.clone());
    }

    @Test
    void Salary() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertEquals(40000, employee.getSalary());
    }
    @Test
    void Name() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertEquals("Равшан", employee.getName());
    }
    @Test
    void generalEmployeeToString() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertEquals("Мое имя - Равшан. Моя должность - сотрудник. Моя зарплата - 40000 Российских рублей.", employee.toString());
    }
    @Test
    void generalEmployee() {
        GeneralEmployee employee = new GeneralEmployee("Равшан", 40000);
        assertEquals("Я один из сотрудников нашей компании", employee.employee());
    }
}

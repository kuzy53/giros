package Task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountantTest {
    @Test
    void accountant() {
        Accountant accountant = new Accountant("Ренат", 80000);
        assertEquals("Я работаю бугалтером", accountant.accountant());
    }

    @Test
    void accountantToString() {
        Accountant accountant = new Accountant("Ренат", 80000);
        assertEquals("Мое имя - Ренат. Моя должность - бухгалтер. Моя зарплата - 80000 Российских рублей.", accountant.toString());
    }
}

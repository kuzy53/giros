package Task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WorkerTest {
    @Test
    void worker() {
        Worker worker = new Worker("Равшан", 40000);
        assertEquals("Я обычный работник", worker.worker());
    }

    @Test
    void workerToString() {
        Worker worker = new Worker("Витя", 40000);
        assertEquals("Мое имя - Витя. Моя должность - работник. Моя зарплата - 40000 Российских рублей.", worker.toString());
    }
}

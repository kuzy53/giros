package Task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChiefAccountantTest {
    @Test
    void chiefAccountant() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Басков", 130000);
        assertEquals("А я - главный среди бугалтеров", chiefAccountant.accountant());
    }

    @Test
    void chiefAccountantToString() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Басков", 130000);
        assertEquals("Мое имя - Басков. Моя должность - главный бухгалтер. Моя зарплата - 130000 Российских рублей.", chiefAccountant.toString());
    }

    @Test
    void chiefAccountantChangeSalary() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Басков", 130000);
        Worker worker = new Worker("Равшан", 40000);
        chiefAccountant.changeSalary(worker, 50000);
        assertEquals(50000, worker.getSalary());
    }
}

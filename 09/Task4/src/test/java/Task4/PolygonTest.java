package Task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {
    @Test
    void perimeterPolygon() {
        int[] sides = new int[]{3, 7, 4, 2, 1};
        Polygon polygon = new Polygon(sides);
        assertEquals(17, polygon.perimeterPol());
    }

    @Test
    void Polygon() {
        int[] sides = new int[]{4, 5, 2, 3, 1};
        Polygon polygon = new Polygon(sides);
        assertEquals("5-угольник, периметр у которого равен - 15", polygon.output());
    }

    @Test
    void triangleCheck() {
        int[] sides = new int[]{1, 2, 3};
        Polygon polygon = new Polygon(sides);
        assertTrue(polygon.checkLen());
    }
}

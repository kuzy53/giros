package Task4;

class Triangle extends Polygon {

    Triangle(int[] edges) {
        super(edges);
    }

    public String output() {
        if (perimet == 0 && sides.length != 3) {
            return "Это не треугольник";
        }
        return "Треугольник, периметр у которого равен - " + this.perimet;
    }
}

package Task4;

class Main {
    public Output[] allPolygons;

    Main(Output[] allPoly) {
        this.allPolygons = allPoly;
    }

    public static void main(String[] args)  {

        int[] sides1 = new int[]{2, 5, 6};
        Triangle triangle1 = new Triangle(sides1);

        int[] sides2 = new int[]{4, 7, 2};
        Triangle triangle2 = new Triangle(sides2);

        int[] sides3 = new int[]{3, 5, 11, 2, 1};
        Polygon polygon = new Polygon(sides3);

        Text text1 = new Text("Надо сдать лабы!");
        Text text2 = new Text("Надо сдать проект!");

        Output[] out = new Output[]{triangle1, triangle2, polygon, text1, text2};
        Main lol = new Main(out);
        lol.printAll();
    }

    public void printAll() {
        for (int i = 0; i < this.allPolygons.length; i++) {
            System.out.println(this.allPolygons[i].output());
        }
    }
}


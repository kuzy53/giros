import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Kuzy53!!!
 */
public class LoveRegular {
    /**
     * Метод, с которого начинается запуск программы!
     * @param args - массив строк
     * @return void
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println();
    }
    /**
     * Метод, заменяющий нелюбимое слово в тексте - любым
     * @param text - текст, в котром мы хотим заменить слова
     * @param notLoveReplace - нелюбимые слова
     * @param LoveWord - любимое
     * @return - строка, в которой слово заменено нужным
     */
    public static String textRegular(String text, String[] notLoveReplace, String LoveWord){

        Pattern pattern = Pattern.compile(String.join("|", notLoveReplace));
        Matcher matcher = pattern.matcher(text);
        return matcher.replaceAll(LoveWord);
    }
}

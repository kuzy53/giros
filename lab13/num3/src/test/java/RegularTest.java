import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegularTest {
    @Test
    void testLove1() {
        assertEquals(LoveRegular.textRegular("Ну че, пацаны, аниме?", new String[]{"аниме"}, "java"),
                "Ну че, пацаны, java?");
    }

    @Test
    void testLove2() {
        assertEquals(LoveRegular.textRegular("Ну че, пацаны, аниме?", new String[]{"аниме", "пацаны"}, "за аниме"),
                "Ну че, за аниме, за аниме?");
    }

}



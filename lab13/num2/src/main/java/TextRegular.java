import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Kuzy53!!!
 */
public class TextRegular {
    /**
     * Метод, с которого начинается запуск программы!
     *
     * @param args - массив строк
     * @return void
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println((getText(text)));
    }

    /**
     * Метод, который выводит слова, стоящие перед числительнымм
     *
     * @param text - предложение
     * @return String
     */
    public static String getText(String text) {
        String regular = ("([П|п]ят(ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми))\\s(\\w+)");
        Pattern pat = Pattern.compile(regular, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher match = pat.matcher(text);
        ArrayList<String> words = new ArrayList<>();
        while (match.find()) {
            words.add(match.group(3));
        }
        return String.join(" ", words);
    }
}




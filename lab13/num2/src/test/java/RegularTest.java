import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RegularTest {
    @Test void textRegularTest1(){
        assertEquals(TextRegular.getText("Я занял пятое место."),"место");
    }

    @Test void textRegularTest2(){
        assertEquals(TextRegular.getText("Я стоял в очереди, пятым номером"),"номером");
    }

    @Test void textRegularTest3(){
        assertEquals(TextRegular.getText("Пятый день моя пятая бабушка лежит в кровати"),"день бабушка");
    }

    @Test void textRegularTest4(){
        assertEquals(TextRegular.getText("Пятому гному подарили Белоснежку"),"гному");
    }
}

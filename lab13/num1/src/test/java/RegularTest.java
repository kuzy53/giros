import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RegularTest {
    @Test
    void telephoneTest1() {
        assertFalse(Regular1.regularPhone("+7(9172634741"));
    }
    @Test
    void telephoneTest2() {
        assertTrue(Regular1.regularPhone("+7-917-263-47-41"));
    }
    @Test
    void telephoneTest3() {
        assertTrue(Regular1.regularPhone("+7(917)246-87-48"));
    }
    @Test
    void telephoneTestFalse() {
        assertFalse(Regular1.regularPhone("+55(2)20644-46-73"));
    }
    @Test
    void telephoneTestBig() {
        assertFalse(Regular1.regularPhone("+55(332)206-468-737"));
    }
    @Test
    void telephoneTestWord() {
        assertFalse(Regular1.regularPhone("+55(332)2-46-FD"));
    }
    @Test
    void telephoneTestEmpty() {
        assertFalse(Regular1.regularPhone(""));
    }

    @Test
    void telephoneTestSpace() {
        assertTrue(Regular1.regularPhone("+7 917 263 47 41"));
    }

}


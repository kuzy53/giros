import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
/**
 * Программа для ввода и обработки пользовательского номера!
 * @author Kuzy53!!!
 */
public class Regular1 {
    /**
     * Метод, с которого начинается запуск программы!
     * @param args - массив строк
     * @return void
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.println(regularPhone(number));
    }
    /**
     * Метод, верифецирующий номер телефона
     * @param phone - строка с номером телефона для бработки
     * @return - boolean
     */
    public static boolean regularPhone(String phone){
        Pattern pattern = Pattern.compile("\\+\\d{1,4}(\\-|\\s)?(\\d{3}|\\(\\d{3}\\))(\\-|\\s)?\\d{3}((\\-|\\s)?\\d{2}){2}");
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

}
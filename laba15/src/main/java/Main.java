import java.util.HashMap;
import java.util.Scanner;

public class Main {

    static Scanner scr = new Scanner(System.in);

    public static void main(String[] args) {
        var number = "";
        String nameFile;

        while (!number.equals("1") && !number.equals("2")) {
            System.out.println("""
                    Choose an option:
                    1. Serializable
                    2. Deserializable""");
            number = scr.nextLine();
        };

        System.out.println("Enter the path to the file");
        nameFile = scr.nextLine();

        if (number.equals("1")){
            HashMap<Integer, Student> studentMap = Student.generateMapStudent();
            Serialize.serializeStudent(nameFile, studentMap);
        } else {
            Serialize.deserializeStudent(nameFile);
        }
    }
}

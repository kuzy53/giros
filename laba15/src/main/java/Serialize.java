import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This is Student.
 * @author kuzy53
 */

public class Serialize {

    /**
     * This is serializeStudent
     * @param pathFile path file
     * @param mapStudent map student
     */
    public static void serializeStudent(String pathFile, Map<Integer, Student> mapStudent){
        try {
            var fileOut = new FileOutputStream(pathFile);
            var objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(mapStudent);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is deserializeStudent
     * @param pathFile path file
     */
    public static void deserializeStudent(String pathFile){
        try {
            var fileInput = new FileInputStream(pathFile);
            var objectInput = new ObjectInputStream(fileInput);
            var studentMap = (HashMap<Integer, Student>) objectInput.readObject();

            for (HashMap.Entry<Integer, Student> student : studentMap.entrySet()) {
                System.out.println(student.getValue());
            }

            objectInput.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
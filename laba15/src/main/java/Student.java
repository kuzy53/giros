import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

/**
 * Student class
 * @author kuzy53
 */
public class Student implements Serializable {

    /** id student */
    private final int id;

    /** name student */
    private final String name;

    /** age student */
    private final int age;

    /** @value Number of key-value pairs HashMap */
    public static final int VALUE = 100;

    /** @value length of the generated string */
    public static final int SIZE_STRING = 10;

    /** @value maximum age */
    public static final int AGE = 50;


    /** control version */
    private static final long serialVersionUID = 88005353535L;

    /**
     * Using this method, you can get the student ID.
     * @param id id
     * @param name name
     * @param age age
     */
    public Student(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    /**
     * Using this method, you can get the student ID.
     * @return Student id
     */
    public int getId(){
        return this.id;
    }

    /**
     * Using this method, you can get the student name.
     * @return Student name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Using this method, you can get the student age.
     * @return Student name
     */
    public int getAge(){
        return this.age;
    }

    /**
     * overridden method toString().
     * @return String
     */
    public String toString(){
        return ("id : " + this.id + "\nname : " + this.name + "\nage : " + this.age);
    }

    /** static method generationRandomString
     * @return сгенерированную строку
     */
    public static String generationRandomString() {
        var name = "";
        var r = new Random();
        for(int i = 0; i < SIZE_STRING; i++) {
            var c = (char)(r.nextInt(26) + 'a');
            name += c;
        }
        return name;

    }

    /** static method generationMap
     * generates a HashMap and fills it in
     * @return instance of the class HashMap
     */
    public static HashMap<Integer, Student> generateMapStudent() {
        var map = new HashMap<Integer, Student>();
        for (int i = 0; i < VALUE; i++) {
            map.put(i+1, new Student(i+1, generationRandomString(), (int) (Math.random() * AGE)));
        }
        return map;
    }

}
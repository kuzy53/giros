number = [1, 2, 5, 7, 4, 3, 6]
swap = true
size = number.length - 1
while swap
    swap = false
    for i in 0...size
        swap |= number[i] > number[i + 1] 
        number[i], number[i + 1] = number[i + 1], number[i] if number[i] > number [i + 1]
    end
    size -= 1
end
puts number.join(', ')
